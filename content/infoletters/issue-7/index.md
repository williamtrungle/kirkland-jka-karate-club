---
title: Poems and nutrition tips
date: 2020-02-20T20:57:36-04:00
issue: 7
summary: >-
  Two new categories additions to our monthly infoletters. Annual Montreal
  Tournament registration opens.
author: sensei_angelo_massotti
draft: false
---

Our two new montly additions from members of our community: a poem of the month
by our most senior member, Le Phung Doan, 70 years young, and a nutrition tip of the month by
our very own Dr. Michele Iskandar, lecturer at the Mcgill University's School of
Human Nutrition.

{{% poem "Le Phung Doan" %}}
### The World of the Mirror Lake at Night

The rows of trees on the opposite shore turn into \
a dark castle wall in which the darker houses hide \
their dark and hard faces.

A lonesome star comes down and kisses a tree \
heeding the tremolos of frog calls \
resounding along the shore.

And a car sweeps its light beams around the curve. \
Looking for answers. But true answers lie \
in the dark and shaky world below the waterline.

And a goose rushes across the lake and lets out a cry. \
As if trying to rescue someone \
who’s drowning in the truth they fail to see.
{{% /poem %}}

## Competitors

March 15th is the date of the annual Montreal Tournament. Are you participating?
Register this week.

## Path of Endeavor

Dan testing will be held on Saturday, April 25th. We are grateful to have Sensei
Shiina Katsutoshi from headquarters in Japan as our examiner. You are encouraged
to take extra classes. See list of officla JKA katas with total movements and
kiai points.
