---
title: Fall session officially begins
summary: >-
  Congratulations to Mark Deschatelets for his 6th Dan and Joanne Robidas for
  her 4th Dan!
issue: 2
author: sensei_angelo_massotti
date: 2019-09-20T15:45:21-05:00
---

Congratulations to: **Mark Deschatelets** for achieving his 6th Dan, a real
accomplishment and inspiration for everyone — **Joanne Robidas** for achieving
her 4th Dan. Determination. We call her bumble bee now.

New Fall 2019 session has started — nice to see everyone back. Registration
numbers are very good for Tuesday, Thursday and for Saturday. An extra week has
been added to the training schedule in December. Therefore, we end the last day
of class on December 12 2019.

[**Update**: Last day of class is on December 17 2019, being a make-up class for
a cancelled class November 26 2019](/infoletters/issue-3)

## Path of Endeavor

{{% row %}}

{{% col %}}
### Our Newest Dan Members
- Mark Descatelets: 6th Dan
- Joanne Robidas: 4th Dan
- William Le: 3rd Dan
- Timothy Lan: 3rd Dan
- Matteo Biscotti: 2nd Dan
- Brenden Sklivas: 1st Dan
- Jonathan Marin: 1st Dan
{{% /col %}}

{{% col %}}
### &nbsp;
The 2019 Canadian Gasshuku was an amazing training experience with participants
coming from across Canada, Ireland, South America and USA. It's really nice to
see Canadians and everyone else train together.
{{% /col %}}

{{% /row %}}
