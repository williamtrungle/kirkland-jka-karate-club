---
title: Halloween
date: 2021-10-28T17:10:29-05:00
issue: 10
summary: Have fun and be safe. There's a little child in all of us!
author: sensei_angelo_massotti
---
So great to have you with us. New comers and returning members. Courageous
dedication in these gallenging times. We are a strong Dojo. Thanks to all your
contributions.

## Coming Soon

Home videos by our own members on practice and exam material will be available
soon on the website.

## Nutrition Tips

Homemade veggie burger recipes, here are some of the best that I found that are
also among the healthiest.

This one shows you the basics and how you can customize your burger based on the
ingredients you like:
https://thecookful.com/veggie-burger-formula-make-perfect-burger/

This one is among the simplest and fewest ingredients:
https://chocolatecoveredkatie.com/veggie-burger-recipe-best/

This one also has a source of omega-3 fats:
https://www.noracooks.com/the-best-black-bean-veggie-burger/

This one has feta cheese in it for an extra flavor twist:
https://sallysbakingaddiction.com/best-black-bean-burgers/

And this one comes highly rated:
https://www.inspiredtaste.net/36554/veggie-burger-recipe/

**\- Michele Iskandar**

---

{{% poem "Dinh Le Doan" %}}
### October Light

Bright sunlight dazzles the leaves \
outside my bedroom window.

A more sober version descends \
upon my clock radio.

Upon which an errant ray of light \
tumbles and scatters.

and yet \
"No burden", it says.

"An errant ray moves on \
without any burden", it says

as it lands on me \
on this October day.
{{% /poem %}}

