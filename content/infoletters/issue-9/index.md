---
title: Lockdown continues
date: 2020-05-14T20:57:39-04:00
issue: 9
summary: Public places remain closed, but I can see the light.
author: sensei_angelo_massotti
draft: false
---

Glad you all received a credit of registration fees from Kirkland. Everything is
still on hold. Let you know as soon as I have more information. Public places
remain closed, but I can see the light. You are in my thoughts.

**New**: Fun and games below.

## Path of Endeavor

Are we like bears? Wow! How hard it is. These are real difficult times for us in
many ways. Certainly, mental. Our journey came to an abrupt stop. But wait,
bears go into hibernation for months. Then when they come out, they are bigger,
stronger with new life. Wow! We will, too. In some ways this will bring on
a revolutionary change. We will create new ways to train karate, even lifestyles
for that matter. Remember we are *Budo* karate. This will mould our character 
foreverand it will serve as a chapter to becoming better people.
