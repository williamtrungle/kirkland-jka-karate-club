---
title: End of Fall 2019 Session
date: 2019-12-20T18:39:01-04:00
issue: 5
summary: >-
  Congratulations to everyone in this fall Session for their courage and
  determination in their pursuit of excellence.
author: sensei_angelo_massotti
---

New session begins for the Winter 2020 session onJanuary 7th for the weekdays
group (Tuesday/Thursday) and January 11th for the weekends group (Saturday). Dan
black belt future examinees are recommended to register for both groups. See you
all then.

For our trip to Japan, Melinda and I trained at Hombu dojo for 1 to 3 hours per
day with headquarters instructors: [Imura, Kurasako, Imamura, Shiina Katsutoshi
& Mai, Ida Rashi, Takahashi, Ueda, Hanzaki, and
Ryota](https://www.jka.or.jp/en/instructors/). Extra-ordinary experience.

## Competitors

Fun was had by all in our annual Kirkland Dojo Tournament. We'll do the Team vs
Team again in the future.

## Path of Endeavor

Congratulations to everyone in this fall Session for their courage and
determination in their pursuit of excellence.
