---
title: Training for the upcoming LSCDN competition
summary: Be sure to make note of the LSCDN and Kirkland tournaments in your agenda!
issue: 3
author: sensei_angelo_massotti
date: 2019-10-20T15:45:22-05:00
---

There will be no class on Tuesday November 26 2019. The make-up class will occur on Tuesday December 17 instead, which officially becomes the last day of class. See you all in great number!

## Competitors

Be sure to make note of the following 2 tournaments in your agenda! November 10 2019: LSCDN and December 14 2019: Kirkland Competition. Thank you to all who have chosen to compete and represent our dojo.

## Path of Endeavor

{{% row %}}

{{% col %}}
### Francois Gelinas Award
Congratulations to Sensei Mark Descatelets for his dedication, passion and resiliency to endeavor.
{{% /col %}}

{{% col %}}
### Special Guest Instructor
On Tuesday November 5, during our regular class, we are fortunate to welcome **Sensei Minoru Saeki, 7th Dan Chief Instructor** from Ottawa JKA & CJKF.
{{% /col %}}

{{% col %}}
### Website
Check out our new, work in progress, JKA Kirkland website. Thank you William Le for all your help.
{{% /col %}}

{{% /row %}}
