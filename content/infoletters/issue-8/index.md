---
title: Covid-19
date: 2020-03-20T20:57:37-04:00
issue: 8
summary: >-
  Everything is on hold and in process of being re-scheduled for a future date.
  Let you know as soon as I have more information. You are in my thoughts.
author: sensei_angelo_massotti
draft: false
---

Dear Karate-kas,
I hope you are all doing well during this Covid-19 period. I'm sure you are all
following recommended safety procedures. We'll all get through this together. Be
safe. Stay active. **Miss you all**. See you soon.

## Path of Endeavor

In these difficult times, we all change channels to meet our challenges. We are
strong. We will become stronger. Appreciate what we have, especially each other.
Mankind is there in time of need.

{{% poem "Dinh Le Doan" %}}
### Do You Remember Still

1 \
Do you remember still our rocky \
hill of childhood that had so captivated \
and challenged us?

Its dark presence threateningly loomed \
in our imagination at night \
and its towering slope stood haughtily \
in defiance of our wishes during the day.

And yet with time we grew stronger \
and we made the difficult climb \
up the hill. Such a daring act. \
Yet we triumphed.

2 \
And as we stood upon the hill looking down \
on some of the same \
frequently roamed places of childhood

no matter how familiar they had been to us \
they now appeared small or narrow \
within the larger view. 
{{% /poem %}}
