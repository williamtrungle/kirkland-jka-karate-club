---
title: First summer session
summary: WOW! First summer session has come and gone; what a great one this one was.
issue: 1
author: sensei_angelo_massotti
date: 2019-08-20T15:11:05-05:00
---
WOW! First summer session has come and gone; what a great one this one was. Emphasis on jyu kumite (free sparring) and shitei, sentei katas. Thank you to all the participants - this would not be possible without all of youse.
