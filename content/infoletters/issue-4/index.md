---
title: Tournaments success and future
summary: Congratulations to all competitors of the LSCDN Tournament. Now we prepare for the upcoming Kirkland Tournament.
issue: 4
author: sensei_angelo_massotti
date: 2019-11-21T17:07:11-05:00
---

So proud of everyone who competed. Check out our new scoreboard below. Congratulations to Mya, Athena and Alexander for winning a gold metal in team kata and receing the 1st place trophy at the LSCDN tournament!

{{% row %}}
{{% col center %}}
### No classes on Tuesday, November 26

- Saturday, December 7: 9-10am group
- Saturday, December 7: 10-11am group
- Tuesday, December 10: 6-7pm group
- Thursday, December 12: 7-8pm group
{{% /col %}}
{{% /row %}}

## Competitors

Make sure not to miss the 2019 Kirkland Tournament happening on December 14. Everyone is invited and encouraged to participate.


## Path of Endeavor

**Minoru Saeki Sensei** visit on November 5th. A wealth of knowledge shared with us during these 2 hours. Everyone was on their best behaviour.
