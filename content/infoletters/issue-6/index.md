---
title: Winter session officially begins
date: 2020-01-20T19:09:58-04:00
issue: 6
summary: >-
  Really nice to see everyone back this session strong and eager. Registration
  numbers are very encouraging. Thanks to you!
author: sensei_angelo_massotti
---

Really nice to see everyone back this session strong and eager. Registration
numbers are very encouraging. Thanks to you!

This year many tournaments are happening, one of which being the JKA Funakoshi
Cup World Championship on October 23rd, 24th and 25th in Japan. This tournament
happens once every three years. More details will follow.

> Along our journey, make sure we all respect our basic values. Truly
> demonstrating them is in the Way of Karate and also practiced outside the dojo.

## Path of Endavor

In this 2020 winter/spring session, many of you are preparing for Dan exams.
They will be held on Saturday, April 25th. The head examiner will be Shiina
Katsutoshi Sensei, from headquarters in Japan. Be sure to practice hard.
