---
title: Infoletters
description: >-
  Monthly updates on dojo activities, as well as general letters
  to the members by our Senseis.
date: 2019-11-19T15:01:05-05:00
banner: white
contrast: dark
---
