---
title: Fall 2021 Session
date: 2021-09-21T16:40:16-04:00
letter: Welcome
summary: >-
  Welcome everyone to the start of this new session.
  We are all glad to have you here.
author: sensei_angelo_massotti
---

Welcome everyone to the start of this new session. We are all glad to have you
here. For beginners and returning students we are going to have an exciting
session filled with plenty of events and special guest instructors. Firstly,
allow me to introduce our qualified instructors for this session;

{{% row %}}
{{% col center %}}
- Angelo Massotti – 6th Dan
- Carole Canaan – 5th Dan
{{% /col %}}

{{% col center %}}
- Timothy Lan – 3rd Dan
- Melinda Massotti – 2nd Dan
- Sabrina Massotti – 1st Dan
{{% /col %}}
{{% /row %}}

For beginners, the objective of this course is to introduce basic elementary
shotokan karate. Our programs are tailored to all ages, genders and levels.
Instructions will be for basic techniques, kata (formal sequence of movements)
& basic kumite (controlled basic non contact fighting). In addition, we help
develop emotional and behavior by prospering with good attitude, concentration,
discipline and respect. We’ll add in some healthy eating habits, too.  For
returning students, you are continuing to develop skills and knowledge as you
work your way to the next higher level.

At Kirkland JKA our focus is on the foundational knowledge and skills that
students will need in order to support mental health and well-being, develop
physical and health literacy, and acquire the commitment and capacity to lead
healthy, active lives. To guide you along the way, you will receive your
personalized Growth and Development Plan, a Practise Guide outlining study of
technical skills(don t get overwhelmed with technical names), a On My Way To
Black Belt, an Agenda Schedule for this session and our Mission Statement.
Should anyone have any questions, please feel free to contact me or address them
to any of our instructors.

We thank you for being here. Rewards await those that take on this lifetime
endeavor.
