---
title: Our mission
description: To develop our dojo members to their greatest potential
banner: yard
author: sensei_angelo_massotti
---

To allow all participants, children, adolescents and adults, an equal
opportunity to study the art of Shotokan JKA karate. We will help our athletes
grow, learn and enjoy themselves, while they use and develop their personal,
physical and intellectual skills. Athletes develop skills, self-esteem,
self-confidence and self-control in order to reach their greatest potential, and
develop lifetime skills that will enchance their lives now, and in the future

Kirkland JKA Karate values the lessons that have long been taught by the
founders and members of the JKA family: the pursuit of excellence and teamwork,
ethical and responsible behaviour in and out of the dojo, adherance to the
spirit of the rules, leadership and strength of character and sportsmanship,
respect of one's opponent, acceptance of victory with humility and acknowledgment
of defeat with grace.

In teaching these lessons to its students, Kirkland JKA Karate instills habits
which will lead athletes to better and healthier lives. Through participation,
students strive for mental and physical excellence, in both practice and
competition, while fully immersing their mind, body and spirit. While winning
is not an end in itself, we believe that efforts and sacrifices by our athletes
will lead them to succeed throughout their lives.

We seek to hire highly qualified instructors and coaches providing them with
the unique opportunity to teach positive lifetime skills and values.
To ensure that the athletes are prepared for the commitment, personal
sacrifice, intensity of practice and perseverance through adversity
needed to be successful in athletics, instructors and coaches must help them
develop and understand the roles that desire, dedication and self-discipline
play in reaching team and individual goals. These are lifetime values that
promote and nurture integrity, pride, loyalty and overall character.

We all take pride in knowing that our qualified instructors are positive
educators and our athletes are true representatives of Kirkland JKA Karate.
Successful athletic teams generate unique excitement across the dojo and
community, help strengthen bonds among the various arms of our dojo, builds
loyalty in a healthy manner and give our community members yet another reason
to be proud to represent Kirkland JKA Karate.

Kirkland JKA Karate Club builds community through the engagement of students,
instructors, parents and supporters by creating a portal through which
neighboring communities can enjoy our experience.
