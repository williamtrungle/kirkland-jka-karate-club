---
title: 9th Kyu
tokui: Taikyoku Shodan (by count)
belt: White belt / yellow stripe
time: 20 classes as 10th kyu
weight: 2
---

{{< vimeo 650526405 >}}

{{< vimeo 650528892 >}}

{{< vimeo 650673602 >}}
