---
title: 10th Kyu
belt: White belt / black stripe
time: 10 classes total
weight: 1
---

#### Kihon
---

1. Jodan choku zuki
    - Maintain straight upper body.
    - Fists must be made properly tight - use first 2 knuckles (Seiken).
    - Proper aim at target - Jinchu for jodan (between nose and upper lip).
    - Shoulders should not be raised or pushed forward to generate power.
    - Punching and pulling motion at same time.
2. Chudan choku zuki
    - See 1.
    - Target area is chudan. Suigetsu is the japanese term for chudan - Solar
      plexus.
3. Jodan age uke
    - Use outer aspect of wrist with a sharp upwards motion.
    - Elbow of the blocking arm should run a course along the side of the body.
    - Arms cross in front of the chin, back of fists facing forward, blocking
      arm on the outside.
    - The raised forearm is parallel to the forehead, slightly on an angle and
      one fist away from the forehead.
4. Chudan soto uke
    - Blocking elbow should be bent at a right angle raised to the outside,
      forearm vertical to floor.
    - Firmly strike with the outer aspect of the wrist, in a circular motion
      from outside to in keeping right angle.
    - The blocking arm elbow position is one fist away from body and fist is at
      shoulder level.
    - Blocking and pulling motion at same time.
    - Cover solar plexus trajectory.
5. Chudan uchi uke
    - Elbow should be bent at right angle.
    - Firmly strike with the inner aspect of the wrist, starting from opposite
      hip in a circular from inside to outside motion keeping right angle.
    - The elbow position is one fist away from body and fist is at shoulder
      level.
    - Blocking and pulling motion at same time.
6. Gedan barai
    - The hand doing the block is positioned at the lower part of the opposite
      ear and blocking downward on an angel, relax the elbow.
    - The hand that is blocking, uses a twisting motion to block with the arm
      extended fully.
    - Outer aspect of the wrist used to block.
    - Blocking and pulling motion at same time.
7. Shuto uke
    - The tip of the fingers forming the knife should be kept tightly.
    - Keep wrists straight.
    - Arm preparation: blocking hand raised to opposite ear area wrist palm
      facing inside, elbow kept down, retracting arm pointing in direction of
      block palm of hand facing down.
    - The knife hand should strike in a downward cutting motion. The other arm
      is pulled back to the solar plexus palm of hand facing upwards.
    - Keep wrists straight.
    - The elbow of the blocking are should stop and be kept along side of the
      body.
    - Eyes look in the direction of the block.
8. Chudan mae geri
    - Upper body and back should be kept straight.
    - To kick, the knee is raised high up with the heel pulled close to the body.
    - Both feet, knees and toes are pointing toward target.
    - There are no lateral movement of the upper body.
    - For kicking foot, ensure to use ball of foot (koshi).
    - Supporting leg is strong, knee slightly bent.
    - Kicking leg knee is relaxed, spring motion and sharp retraction.
9. Chudan yoko geri keage
    - The foot that is kicking should be brought up high and tucked in, knee
      pointing sideways in the direction of the target.
    - Use the kicking knee as a pivot point and with a spring motion, draw
      a half circle snapping back foot.
    - Ensure to use side of foot (sokuto).
    - When kicking do not raise the heel of the supporting foot.
    - The upper body is not twisted or tipped backwards.
    - The knee of the supporting leg should be solid in its support.
    - Look in direction of kick.
10. Chudan yoko geri kekomi
    - The foot that is kicking should be brought up high and tucked in resting
      on inside of supporting leg.
    - Use the side of the foot (sokuto) to kick in a straight line, sideways.
    - There should be a feeling of the knee thrusting into its target.
    - Kicking and pulling back the foot should follow same trajectory.
    - The supportive leg should be strong and firm.
    - Do not raise the heel of the foot of the supporting leg.
    - Look in direction of kick.

{{< vimeo 650466023 >}}
