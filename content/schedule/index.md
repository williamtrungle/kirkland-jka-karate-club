---
headless: true
title: Information
---
Registration period begins **08/26** for Kirkland residents, on **08/30** for Beaconsfield residents, and on **09/01** for non-residents. A uniform must be purchased at additional cost, one per student, from the lead instructor. Please note that a parent may register with his/her child.
