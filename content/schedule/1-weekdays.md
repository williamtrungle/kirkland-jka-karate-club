---
title: Weekdays
location: Gymnasium
schedule:
- ["Tuesday: 18:00 - 19:00", Beginners]
- ["Tuesday: 19:00 - 20:00", Advanced]
- ["Thursday: 18:00 - 19:00", Beginners]
- ["Thursday: 19:00 - 20:00", Advanced]
banner: receiving-medal
---
Fall 2022 session from **09/13** to **12/18**. Sweatpants and t-shirts are permitted until acquiring a uniform.

Beginners include: *white*, *yellow*, *orange*, and *green* belts. Advanced includes: *blue/purple*, *brown*, and *black* belts 
