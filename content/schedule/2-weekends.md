---
title: Weekends
location: Reception Hall
schedule:
- ["Saturday: 09:00 - 10:00", Beginner]
- ["Saturday: 10:00 - 11:00", Intermediate]
- ["Saturday: 11:00 - 12:00", Advance]
banner: group-photo
---
Fall 2022 session from **09/17** to **12/18**. Sweatpants and t-shirts are permitted until acquiring a uniform.

Beginner includes: *white*, and *black* belts. Intermediate includes: *white*, *yellow*, *orange*, and *green* belts. Advance includes: *blue/purple*, *brown*, and *black* belts.
