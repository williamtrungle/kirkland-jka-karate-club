---
date: 1985-07-07T19:22:11-04:00
description: The Secret is in the Training
title: Kirkland JKA Karate Club
banner: japanese-courtyard
---

## Who we are

Founded in 1985 by Sensei Francois Gelinas, our Shotokan dojo is currently headed by Sensei Angelo Massotti and is a proud affiliate of the internationally recognized Japan Karate Association through our membership of the JKA-SKD Canada.

[jkaskd.amassotti@gmail.com](mailto:jkaskd.amassotti@gmail.com)
