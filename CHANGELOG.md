# Changelog
This file documents major all notable changes to the website layout, design, and contents, starting at the first semantic major version 1.0.0.

## [1.5.0] - 2019-12-01
### Added
- Infoletters list page and single pages.
- Shortcode for column layout in post markdown.
- Support for alternate font color in footer from front matter.
- Archetype for infoletter pages.

### Changed
- Content size width and spacing.
- Paragraph and dropcaps spacing.

### Fixed
- Raw html not working in nested shortcodes due to Hugo upgrade to v0.60.

## [1.4.0] - 2019-11-18
### Added
- Hamburger button and fullscreen menu in mobile mode

## [1.3.4] - 2019-11-18
### Changed
- Sroll-to-top button position in mobile mode
- Hero size being true fullscreen in mobile

## [1.3.3] - 2019-11-18
### Removed
- Dropcap appearing on home page schedule section

## [1.3.2] - 2019-11-18
### Added
- Animations to about us page

## [1.3.1] - 2019-11-18
### Fixed
- Contact nav button not scrolling down smoothly

## [1.3.0] - 2019-11-18
### Added
- Dropcap to all article contents
- Add author name/avatar to about page in column layout

### Changed
- Title of about page to "About Us" and put "our mission" in subtitle instead

## [1.2.0] - 2019-11-18
### Fixed
- Styling of footer title not being applied on non-home page
- Hero image height now takes into account whole page padding and aligns correctly
### Changed
- Hero image is now available on all pages and defaults to home's if banner is not defined
- Lazy loading script is made available on all pages
### Added
- 404 page
- About page containing mission statement
- Functional links in nav bar which will default to 404 until the pages are created

## [1.1.1] - 2019-11-15
### Fixed
- Scroll up and contact buttons having absolute links to home page instead of relative links to elements by id.

## [1.1.0] - 2019-11-14
### Added
- Scroll back to the top of page button.
- Smooth scrolling option for anchors to elements using Javascript.
### Changed
- Upcoming data date format to support Hugo date formatting functions.
- Main element have main has id/name and home as a fomatting class.
### Fixed
- Discover and Contact button on home page/nav bar correctly scroll to the relevant element, after adding both id and name attributes for compatibility coverage.

## [1.0.0] - 2019-11-14
### Added
- Description section and email of lead instructor.
- Upcoming events listing.
- Class schedule dates/times description.
- Google Maps plugin showing the dojo address in the footer.
- Repeated call-to-action button in footer.
- Contact info in footer with social media icons.
- Copyright in footer.
- Above-the-fold animations on page load.
- Favicon in single png format.
- Lazy loading of image contents using LazyLoad CDN.
- FontAwesome social media icons.
- 99% performance compliance on Lighthouse and 100% compliance for accessibility, best practices and SEO.
### Changed
- Use a japanese courtyard hero image instead of skyscrapper.
- Nav bar with text-only logo and skeleton nav links.
- Renamed image contents with descriptive file names for alt text.
### Removed
- Pricing section.
- Gallery section.
### Fixed
- Responsiveness in mobile mode causing accidental overflow.

[1.5.0]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.4.0...v1.5.0
[1.4.0]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.3.4...v1.4.0
[1.3.4]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.3.3...v1.3.4
[1.3.3]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.3.2...v1.3.3
[1.3.2]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.3.1...v1.3.2
[1.3.1]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.3.0...v1.3.1
[1.3.0]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/williamtrungle/kirkland-jka-karate-club/compare/v0.3.0...v1.0.0
