# Kirkland JKA

[![Screenshot](screenshot.png)](https://kirklandjka.com)

---
This is the source code for the [Kirkland JKA dojo's website](https://kirklandjka.com).
It is built using Hugo, uses SASS and Javascript for LazyLoad, FontAwesome, and smooth-scrolling.
It is currently hosted and built automatically using Gitlab CI/CD.

## Contributors
- Maintainer: [William Le](mailto:williamtrungle@gmail.com)
- Inquiries: [Kirkland JKA](mailto:kirklandjka.karateclub@gmail.com)
- Head Instructor: [Angelo Massotti](mailto:jkaskd.amassotti@gmail.com)
